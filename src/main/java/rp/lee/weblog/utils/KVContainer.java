/**
 * 
 */
package rp.lee.weblog.utils;

/**
 * @title Container.java
 *
 * @author Andy
 * @email spring.h@aliyun.com
 * @version 1.0
 * @created 2016年12月3日 下午3:34:51
 */
public class KVContainer<K, V> {
	
	private K key;
    
	private V value;
    
	public KVContainer(K k, V v){
		this.key = k;
		this.value = v;
	}
	
	/**
	 * @return the key
	 */
	public K getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(K key) {
		this.key = key;
	}
	/**
	 * @return the value
	 */
	public V getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(V value) {
		this.value = value;
	}
    
}
