package rp.lee.weblog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.iih5.netbox.message.StringMessage;
import com.iih5.netbox.session.ISession;

public class TailLogThread extends Thread {
	private BufferedReader reader;
	private ISession session;
	
	public TailLogThread(InputStream in, ISession session) {
		this.reader = new BufferedReader(new InputStreamReader(in));
		this.session = session;
	}
	
	@Override
	public void run() {
		String line;
		try {
			while((line = reader.readLine()) != null) {
				// 将实时日志通过WebSocket发送给客户端，给每一行添加一个HTML换行
				StringMessage message = new StringMessage((short)1000);
				message.setContent(line + "<br>");
				session.send(message);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
