package rp.lee.weblog;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iih5.netbox.annotation.InOut;
import com.iih5.netbox.core.ConnectExtension;
import com.iih5.netbox.message.StringMessage;
import com.iih5.netbox.session.ISession;

/**
 * 
 * title: ConnectOrDisconnectExtension.java 
 * description
 *
 * @author rplees
 * @email rplees.i.ly@gmail.com
 * @version 1.0  
 * @created Mar 29, 2017 5:00:22 PM
 */
@InOut("connect/disconnect callback")
public class ConnectOrDisconnectExtension extends ConnectExtension {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	public void connect(ISession session) {
		session.setParameter("connectionTime", System.currentTimeMillis() / 1000);
		logger.info("建立链接:{}", session.getId());
		
		StringMessage retMsg = new StringMessage((short)1000);
		retMsg.setContent("hello world!");
        session.send(retMsg);
	}

	public void disConnect(ISession session) {
		LogHandler.clear(session);
		session.unBindUserID(session.getUserID());
		
		long now = System.currentTimeMillis() / 1000;
		long connectionTime = now;
		Object parameter = session.getParameter("connectionTime");
		session.clearParameters();
		if(parameter != null) {
			connectionTime = (Long) parameter;
		}
		logger.info("断开连接:{}, 此次连接了{}秒.", session.getId(), (now - connectionTime));
	}
}